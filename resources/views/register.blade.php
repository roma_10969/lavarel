<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
    @csrf
        <label>First Name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label>Last Name:</label> <br><br>
        <input type="text" name="lname"> <br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label> <br><br>
        <select name="nation">
            <option value="idn">Indonesia</option>
            <option value="sin">Singapore</option>
            <option value="mal">Malaysia</option>
            <option value="aus">Australia</option>
        </select><br><br>
        <label>Languange Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit">
    </form>
</body>
</html>